-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: map
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `positionx` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `positiony` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Location 0','55.91987','37.32561'),(2,'Location 1','55.56904','37.47379'),(3,'Location 2','55.4134','37.09233'),(4,'Location 3','55.96224','37.46089'),(5,'Location 4','55.99246','37.58479'),(6,'Location 5','55.48488','37.19776'),(7,'Location 6','55.57518','37.33306'),(8,'Location 7','55.40862','37.11565'),(9,'Location 8','55.99372','37.2899'),(10,'Location 9','55.94319','37.0684'),(11,'Location 10','55.42074','37.08963'),(12,'Location 11','55.84627','37.61489'),(13,'Location 12','55.72658','37.11957'),(14,'Location 13','56.01184','37.29689'),(15,'Location 14','55.58567','37.54093'),(16,'Location 15','55.39601','37.152'),(17,'Location 16','55.48232','37.26151'),(18,'Location 17','55.89158','37.19219'),(19,'Location 18','55.61964','37.50028'),(20,'Location 19','55.91887','37.25855'),(21,'Location 20','55.70086','37.70021'),(22,'Location 21','55.72211','37.5725'),(23,'Location 22','55.64906','37.4272'),(24,'Location 23','55.95395','37.28923'),(25,'Location 24','55.9829','37.54359'),(26,'Location 25','55.62343','37.7001'),(27,'Location 26','55.89901','37.11616'),(28,'Location 27','55.93077','37.27205'),(29,'Location 28','55.50152','37.58907'),(30,'Location 29','55.83473','37.38366'),(31,'Location 30','55.74578','37.52721'),(32,'Location 31','55.80145','37.52457'),(33,'Location 32','55.4045','37.33948'),(34,'Location 33','55.98255','37.32061'),(35,'Location 34','55.45555','37.54788'),(36,'Location 35','55.84496','37.45287'),(37,'Location 36','55.86387','37.21352'),(38,'Location 37','55.64115','37.15939'),(39,'Location 38','55.90652','37.24155'),(40,'Location 39','55.71441','37.53587'),(41,'Location 40','55.40093','37.63431'),(42,'Location 41','55.85176','37.5964'),(43,'Location 42','56.01626','37.42898'),(44,'Location 43','55.48423','37.16423'),(45,'Location 44','55.63384','37.61543'),(46,'Location 45','55.81368','37.67608'),(47,'Location 46','55.75842','37.26159'),(48,'Location 47','55.81644','37.45939'),(49,'Location 48','55.86687','37.44545'),(50,'Location 49','55.39578','37.61888'),(51,'Location 50','55.60911','37.53721'),(52,'Location 51','55.68753','37.11944'),(53,'Location 52','56.01652','37.62515'),(54,'Location 53','55.54462','37.5695'),(55,'Location 54','55.48249','37.5555'),(56,'Location 55','55.72116','37.17988'),(57,'Location 56','55.80559','37.21937'),(58,'Location 57','55.39206','37.4683'),(59,'Location 58','55.91415','37.17276'),(60,'Location 59','55.89833','37.19444'),(61,'Location 60','55.40398','37.35847'),(62,'Location 61','55.48631','37.45887'),(63,'Location 62','55.88585','37.59921'),(64,'Location 63','55.53404','37.39917'),(65,'Location 64','55.66045','37.22629'),(66,'Location 65','55.63384','37.56603'),(67,'Location 66','56.02929','37.61784'),(68,'Location 67','55.95126','37.69227'),(69,'Location 68','55.85877','37.14234'),(70,'Location 69','55.87755','37.63773'),(71,'Location 70','55.96363','37.24517'),(72,'Location 71','55.43339','37.41568'),(73,'Location 72','55.73033','37.12192'),(74,'Location 73','55.49977','37.29093'),(75,'Location 74','55.56047','37.69456'),(76,'Location 75','55.75116','37.26092'),(77,'Location 76','55.66881','37.53393'),(78,'Location 77','55.98558','37.20112'),(79,'Location 78','55.74893','37.16607'),(80,'Location 79','55.86609','37.70585'),(81,'Location 80','55.65816','37.14638'),(82,'Location 81','55.88766','37.33391'),(83,'Location 82','56.03002','37.48538'),(84,'Location 83','55.64196','37.53525'),(85,'Location 84','55.89352','37.16597'),(86,'Location 85','55.78876','37.5036'),(87,'Location 86','55.67693','37.51861'),(88,'Location 87','55.53507','37.70372'),(89,'Location 88','55.90633','37.3313'),(90,'Location 89','55.61044','37.11325'),(91,'Location 90','55.64164','37.65807'),(92,'Location 91','55.63997','37.60692'),(93,'Location 92','55.80778','37.272'),(94,'Location 93','55.42383','37.20317'),(95,'Location 94','55.70387','37.58638'),(96,'Location 95','55.52481','37.65849'),(97,'Location 96','55.99856','37.05893'),(98,'Location 97','55.60818','37.67504'),(99,'Location 98','55.81011','37.54661'),(100,'Location 99','55.82607','37.35008');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-06 16:54:40
