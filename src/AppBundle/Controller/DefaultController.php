<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Location;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage", defaults={"page": "1"}, requirements={"page": "\d+"})
     */
    public function indexAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Location');
        $page = $request->get('page');

        $locations = $repository->findAllPager($page);

        return $this->render('default/index.html.twig', [
            'locations' => $locations]);
    }

    /**
     * @Route("/search", name="search", defaults={"page": "1"}, requirements={"page": "\d+"})
     */
    public function searchAction(Request $request)
    {
        $page = $request->get('page');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Location');

        $search_form = $this->createFormBuilder([], array(
                'action' => '/search',
                'method' => 'GET'
            ))
            ->add('min_x', HiddenType::class)
            ->add('min_y', HiddenType::class)
            ->add('max_x', HiddenType::class)
            ->add('max_y', HiddenType::class)
            ->add('search', SubmitType::class, array('label' => 'Search'))
            ->getForm();

        $search_form->handleRequest($request);

        if ($search_form->isSubmitted() && $search_form->isValid()) {
            $search_data = $search_form->getData();
            $locations = $repository->searchPager($search_data, $page);
        } else {
            $locations = $repository->findAllPager($page);
        }

        return $this->render('default/search.html.twig', [
            'locations' => $locations,
            'search_form' => $search_form->createView()]);
    }

    /**
     * @Route("/location", name="location_show", defaults={"page": "1"}, requirements={"page": "\d+"})
     */
    public function showAction(Request $request)
    {
        $page = $request->get('page');
        $repository = $this->getDoctrine()->getRepository('AppBundle:Location');

        $locations = $repository->findAllPager($page);

        return $this->render('default/location_show.html.twig', 
            ['locations' => $locations]);
    }

    /**
     * @Route("/location/new", name="location_new")
     */
    public function createAction(Request $request)
    {
        $location = new Location();

        $form = $this->createFormBuilder($location)
            ->add('name', TextType::class)
            ->add('positionx', TextType::class)
            ->add('positiony', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Location'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $location = $form->getData();

            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($location);
            $em->flush();

            $this->addFlash(
                'message',
                ['type' => 'success',
                    'text' => 'Location was successfully added.']);

            return $this->redirect($this->generateUrl(
                'location_update',
                ['id' => $location->getId()]));
        }

        return $this->render('default/location_new.html.twig', [
            'form' => $form->createView()]);
    }

    /**
     * @Route("/location/{id}", name="location_update", requirements={"id": "\d+"})
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $location = $em->getRepository('AppBundle:Location')->find($id);

        if (!$location) {
            throw $this->createNotFoundException('The location does not exist.');
        }

        $form = $this->createFormBuilder($location)
            ->add('name', TextType::class)
            ->add('positionx', TextType::class)
            ->add('positiony', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Update Location'))
            ->getForm();

        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()){
                $em->flush();

                $this->addFlash(
                    'message',
                    ['type' => 'success',
                        'text' => 'Location was successfully updated.']);
            } else {
                $this->addFlash(
                    'message',
                    ['type' => 'error',
                            'text' => 'Location was not updated.']);
            }

        }
  
        return $this->render('default/location_update.html.twig', 
            ['form' => $form->createView()]);
    }

    /**
     * @Route("/location/{id}/delete", name="location_delete", requirements={"id": "\d+"})
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $location = $em->getRepository('AppBundle:Location')->find($id);

        if (!$location) {
            throw $this->createNotFoundException('The location does not exist.');
        }

        $em->remove($location);
        $em->flush();

        $this->addFlash(
            'message',
            ['type' => 'success',
                    'text' => 'Location was successfully deleted.']);

        return $this->redirectToRoute('location_show');
    }

}
