<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Adapter\ArrayAdapter;

/**
 * LocationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LocationRepository extends EntityRepository
{
    public function findAllPager($page = 1, $perpage = 10)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM AppBundle:Location p ORDER BY p.id ASC'
            );
        $adapter = new DoctrineORMAdapter($query);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($perpage);

        try {
            $pagerfanta->setCurrentPage($page);
        } catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $pagerfanta;
    }

    public function searchPager($search_data, $page = 1, $perpage = 10)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT p
            FROM AppBundle:Location p
            WHERE (p.positionx > :min_positionx
            AND p.positionx < :max_positionx
            AND p.positiony > :min_positiony
            AND p.positiony < :max_positiony)'
        )->setParameter('min_positionx', $search_data['min_x'])
        ->setParameter('max_positionx', $search_data['max_x'])
        ->setParameter('min_positiony', $search_data['min_y'])
        ->setParameter('max_positiony', $search_data['max_y']);

        $locations = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        $radius = ($search_data['max_y'] - $search_data['min_y'])/2.0;
        $x0 = $search_data['min_x'] + $radius;
        $y0 = $search_data['min_y'] + $radius;

        foreach($locations as $k => $l) {
            
            $sqrtx = bcpow($l['positionx'] - $x0, 2, 10);
            $sqrty = bcpow($l['positiony'] - $y0, 2, 10);
            $radius_check = $sqrtx + $sqrty;

            if( abs($radius_check - bcpow($radius, 2, 10)) > 0.00001 && abs($radius_check - bcpow($radius, 2, 10)) < 0.000015) {
                unset($locations[$k]);
            }
        }

        $adapter = new ArrayAdapter($locations);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($perpage);
        
        try {
            $pagerfanta->setCurrentPage($page);

        } catch(NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        return $pagerfanta;
    }
}
