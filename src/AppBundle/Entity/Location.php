<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 */
class Location
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $positionx;

    /**
     * @var string
     */
    private $positiony;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Location
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set positionx
     *
     * @param string $positionx
     * @return Location
     */
    public function setPositionx($positionx)
    {
        $this->positionx = $positionx;

        return $this;
    }

    /**
     * Set positionx
     *
     * @param string $positionx
     * @return Location
     */
    public function setPositiony($positiony)
    {
        $this->positiony = $positiony;

        return $this;
    }

    /**
     * Get positionx
     *
     * @return string 
     */
    public function getPositionx()
    {
        return $this->positionx;
    }

    /**
     * Get positiony
     *
     * @return string 
     */
    public function getPositiony()
    {
        return $this->positiony;
    }
}
