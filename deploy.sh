#!/bin/bash

export SYMFONY_ENV=prod;

composer install --no-dev --optimize-autoloader &&

php app/console doctrine:database:drop --force;
php app/console doctrine:database:create &&
php app/console doctrine:schema:update --force &&
php app/console cache:clear --no-warmup --env=prod --no-debug;

echo  "<VirtualHost *:80>" > /etc/apache2/sites-available/000-default.conf;
echo  "      ServerAdmin webmaster@localhost" >> /etc/apache2/sites-available/000-default.conf;
echo  "      DocumentRoot /var/www/map/web" >> /etc/apache2/sites-available/000-default.conf;
echo  "      ErrorLog /var/www/map-error.log" >> /etc/apache2/sites-available/000-default.conf;
echo  "      CustomLog /var/www/map-access.log combined" >> /etc/apache2/sites-available/000-default.conf;
echo  "</VirtualHost>" >> /etc/apache2/sites-available/000-default.conf;

echo  "<Directory /var/www/map/>" >> /etc/apache2/apache2.conf;
echo  "        Options Indexes FollowSymLinks MultiViews" >> /etc/apache2/apache2.conf;
echo  "        AllowOverride All" >> /etc/apache2/apache2.conf;
echo  "        Require all granted" >> /etc/apache2/apache2.conf;
echo  "</Directory>" >> /etc/apache2/apache2.conf;

service apache2 restart;

chown -R www-data:www-data ../map
