Сервер Ubuntu 14.04

```
sudo apt-get update
sudo apt-get install apache2
sudo apt-get install mysql-server php5-mysql
sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt
sudo a2enmod rewrite
sudo service apache2 restart
sudo apt-get install git
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
sudo cd /var/www/

sudo git clone https://grvldmr@bitbucket.org/grvldmr/map.git

sudo cd map
sudo ./deploy.sh

mysql -u*USERNAME* -p *database_name* < dump.sql
```